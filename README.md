# 今日导航

#### 介绍
一个轻便简单可自定义的网址导航

#### 软件架构
后端采用thinkphp框架编写

演示：https://www.youizhu.com

#### 安装教程

1.  后台入口：http://domain/login.html
2.  初始用户名和密码：admin/12345678


#### 使用说明及更新说明

本系统主打简单轻便，没有太多复杂的功能。后续有好的idea会陆续更新

更新预告：

1.目前会员系统比较仓促，下次会写完整

2.前端开起来略复杂，准备在后台添加一个开关，控制是否只保留搜索框

3.优化一些代码提高效率



#### 其他说明

1.  此为开源软件，免费使用
2.  未经许可禁止商用
