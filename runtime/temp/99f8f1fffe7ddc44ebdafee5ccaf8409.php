<?php if (!defined('THINK_PATH')) exit(); /*a:1:{s:85:"E:\phpstudy_pro\WWW\www.youizhu.com\public/../application/index\view\index\index.html";i:1602915468;}*/ ?>
<!DOCTYPE html>
<html lang="zh">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="author" content="viggo" />
    <title><?php echo $info['title']; ?></title>
    <meta name="keywords" content="<?php echo $info['keywords']; ?>">
    <meta name="description" content="<?php echo $info['description']; ?>">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <link href="font/embed.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <script src="https://at.alicdn.com/t/font_1592760_frznr49dvo.js"></script>
    <script src="https://at.alicdn.com/t/font_1592665_8irfenx66gf.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery/dist/jquery.min.js"></script>
    <script src="js/sou.js"></script>
    <link rel="shortcut icon" href="img/logo.ico" />
    <link rel="stylesheet" href="css/search.css" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Arimo:400,700,400italic">
    <link rel="stylesheet" href="/static/index/assets/css/fonts/linecons/css/linecons.css">
    <link rel="stylesheet" href="/static/index/assets/css/fonts/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/index/assets/css/bootstrap.css">
    <link rel="stylesheet" href="/static/index/assets/css/xenon-core.css">
    <link rel="stylesheet" href="/static/index/assets/css/xenon-components.css">
    <link rel="stylesheet" href="/static/index/assets/css/xenon-skins.css">
    <link rel="stylesheet" href="/static/index/assets/css/nav.css">
    <script src="/static/index/assets/js/jquery-1.11.1.min.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body class="page-body">
    <!-- skin-white -->

    <div class="page-container">


        <div class="main-content">
            <nav class="navbar user-info-navbar" role="navigation">
                <!-- User Info, Notifications and Menu Bar -->
                <!-- Left links for user info navbar -->
                <ul class="user-info-menu left-links list-inline list-unstyled" style="margin-left: 30px; margin-top: 15px;">
                    <div class="logo">
                        <a href="/" class="logo-expanded">
                            <img src="<?php echo $info['logo']; ?>" width="25%" alt=""/>
                        </a>
                    </div>
                </ul>
                
                <ul class="user-info-menu right-links list-inline list-unstyled">
                    <li class="layui-nav-item">
                        <a href="javascript:;"><?php echo $user['user_name']; ?></a>
                    </li>
                    <?php if(empty($user['user_name'])): ?>
                    <li class="hidden-sm hidden-xs">
                        <a href="/user/login.html" target="_blank">
                           登录以自定义您的网址
                        </a>
                    </li>
                    <?php endif; ?>
                    <li class="hidden-sm hidden-xs">
                        <a href="/user/outlogin.html" target="_blank">
                           退出
                        </a>
                    </li>
                   
                </ul>
                

            </nav>


            <div id="content">
                <div class="con">
                    <div class="shlogo">
                        <div class="logo-text embed">简洁&nbsp;&nbsp;搜索</div>
                    </div>
                    <section class="sousuo">
                        <div class="search">
                            <div class="search-box">
                                <svg class="icon" id="search-icon" aria-hidden="true">
                                    <use xlink:href="#icon-baidu"></use>
                                </svg>
                                <input type="text" id="txt" class="search-input" autocomplete="off" autofocus="autofocus" placeholder="百度搜索" />
                                <svg class="icon" aria-hidden="true" id="search-clear" style="display: none;">
                                    <use xlink:href="#icon-shanchu"></use>
                                </svg>
                            </div>
                            <!-- 搜索热词 -->
                            <div class="box search-hot-text" id="box" style="display: none;">
                                <ul></ul>
                            </div>
                            <!-- 搜索引擎 -->
                            <div class="search-engine" style="display: none;">
                                <div class="search-engine-head">
                                    <strong class="search-engine-tit">选择您的默认搜索引擎：</strong>
                                    <div class="search-engine-tool">
                                        搜索热词：<label class="switch">
                                            <input id="hot-btn" type="checkbox">
                                            <div class="slider round"></div>
                                        </label>
                                    </div>
                                </div>
                                <ul class="search-engine-list"></ul>
                            </div>
                        </div>
                    </section>
                </div>
                <br />
            <!-- 个人自定义 -->
            <?php if(!empty($user)): ?>
            <h4 class="text-gray"><i class="linecons-tag" style="margin-right: 7px;" id="zidingyi"></i>个人导航</h4>
            <?php endif; ?>
            <div class="row">
                <?php foreach($zidingyi as $zidingyi): ?> 
                <div class="col-sm-3">
                    <div class="xe-widget xe-conversations box2 label-info" onclick="window.open('<?php echo $zidingyi["link_address"]; ?>', '_blank')" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php echo $zidingyi['link_address']; ?>">
                        <div class="xe-comment-entry">
                            <a class="xe-user-img">
                                <img data-src="<?php echo $zidingyi['link_icon']; ?>" class="lozad img-circle" width="16">
                            </a>
                            <div class="xe-comment">
                                <a href="#" class="xe-user-name overflowClip_1">
                                    <strong><?php echo $zidingyi['link_name']; ?></strong>
                                </a>
                                <p class="overflowClip_2"><?php echo $zidingyi['link_note']; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
            <br />
            
            <!-- 常用推荐 -->
            <?php foreach($list as $sumlist): ?>
            <h4 class="text-gray"><i class="linecons-tag" style="margin-right: 7px;" id="<?php echo $sumlist['type_name']; ?>"></i><?php echo $sumlist['type_name']; ?></h4>
            <div class="row">
            <?php if(empty($sumlist['arr'])): else: foreach($sumlist['arr'] as $mlist): foreach($mlist as $li): ?>
                <div class="col-sm-3">
                    <div class="xe-widget xe-conversations box2 label-info" onclick="window.open('<?php echo $li["link_address"]; ?>', '_blank')" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="<?php echo $li['link_address']; ?>">
                        <div class="xe-comment-entry">
                            <a class="xe-user-img">
                                <img data-src="<?php echo $li['link_icon']; ?>" class="lozad img-circle" width="16">
                            </a>
                            <div class="xe-comment">
                                <a href="#" class="xe-user-name overflowClip_1">
                                    <strong><?php echo $li['link_name']; ?></strong>
                                </a>
                                <!-- <p class="overflowClip_2"><?php echo $li['link_note']; ?></p> -->
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach; endforeach; endif; ?>
            </div>
            <br />
            <?php endforeach; ?>
            
            <!-- END 常用推荐 -->

            
            
        </div>
    </div>
    <!-- 锚点平滑移动 -->
    <script type="text/javascript">
    $(document).ready(function() {
         //img lazy loaded
         const observer = lozad();
         observer.observe();

        $(document).on('click', '.has-sub', function(){
            var _this = $(this)
            if(!$(this).hasClass('expanded')) {
               setTimeout(function(){
                    _this.find('ul').attr("style","")
               }, 300);
              
            } else {
                $('.has-sub ul').each(function(id,ele){
                    var _that = $(this)
                    if(_this.find('ul')[0] != ele) {
                        setTimeout(function(){
                            _that.attr("style","")
                        }, 300);
                    }
                })
            }
        })
        $('.user-info-menu .hidden-sm').click(function(){
            if($('.sidebar-menu').hasClass('collapsed')) {
                $('.has-sub.expanded > ul').attr("style","")
            } else {
                $('.has-sub.expanded > ul').show()
            }
        })
        $("#main-menu li ul li").click(function() {
            $(this).siblings('li').removeClass('active'); // 删除其他兄弟元素的样式
            $(this).addClass('active'); // 添加当前元素的样式
        });
        $("a.smooth").click(function(ev) {
            ev.preventDefault();

            public_vars.$mainMenu.add(public_vars.$sidebarProfile).toggleClass('mobile-is-visible');
            ps_destroy();
            $("html, body").animate({
                scrollTop: $($(this).attr("href")).offset().top - 30
            }, {
                duration: 500,
                easing: "swing"
            });
        });
        return false;
    });

    var href = "";
    var pos = 0;
    $("a.smooth").click(function(e) {
        $("#main-menu li").each(function() {
            $(this).removeClass("active");
        });
        $(this).parent("li").addClass("active");
        e.preventDefault();
        href = $(this).attr("href");
        pos = $(href).position().top - 30;
    });
    </script>
    <!-- Bottom Scripts -->
    <script src="/static/index/assets/js/bootstrap.min.js"></script>
    <script src="/static/index/assets/js/TweenMax.min.js"></script>
    <script src="/static/index/assets/js/resizeable.js"></script>
    <script src="/static/index/assets/js/joinable.js"></script>
    <script src="/static/index/assets/js/xenon-api.js"></script>
    <script src="/static/index/assets/js/xenon-toggles.js"></script>
    <!-- JavaScripts initializations and stuff -->
    <script src="/static/index/assets/js/xenon-custom.js"></script>
    <script src="/static/index/assets/js/lozad.js"></script>
    <script src=" ./js/search.js"> </script>
    <script src=" ./js/jsonload.js"> </script>
    <script>
        var _hmt = _hmt || [];
        (function() {
          var hm = document.createElement("script");
          hm.src = "https://hm.baidu.com/hm.js?1ca60cb2e06a030cc2f2afb976a364f1";
          var s = document.getElementsByTagName("script")[0]; 
          s.parentNode.insertBefore(hm, s);
        })();
        </script>
        
</body>

</html>
